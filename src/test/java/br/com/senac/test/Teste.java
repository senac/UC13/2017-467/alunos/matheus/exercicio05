
package br.com.senac.test;

import br.com.senac.moeda.Moedas;
import org.junit.Test;
import static org.junit.Assert.*;


public class Teste {
    
    public Teste() {
    }
    
    
    @Test
    public void deveConverter10ReaisPara2E45DollarAmericano(){
        Moedas conversorDeMoedas = new Moedas();
        double real = 10 ; 
        
        double resultado  = conversorDeMoedas.virandoDolar(real);
        assertEquals(2.45, resultado , 0.01);
    }
    
    @Test
    public void deveConverter10ReaisPara3E38DollarAustraliano(){
        Moedas conversorDeMoedas = new Moedas();
        double real = 10 ; 
        
        double resultado  = conversorDeMoedas.virandoAustralia(real);
        assertEquals(3.38, resultado , 0.01);
        
    }
    
     @Test
    public void deveConverter10ReaisPara2E08Euros(){
        Moedas conversorDeMoedas = new Moedas();
        double real = 10 ; 
        
        double resultado  = conversorDeMoedas.virarEuro(real);
        assertEquals(2.08, resultado , 0.01);
        
    }
    
    
    
}
